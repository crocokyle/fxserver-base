# syntax=docker/dockerfile:1
FROM ubuntu:focal
RUN mkdir ~/FXServer
RUN mkdir ~/FXServer/server
RUN mkdir ~/FXServer/server-data
RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y npm nodejs
RUN apt-get update \
  && apt-get install -y wget git xz-utils python3.4 python3-pip npm nodejs \
  && rm -rf /var/lib/apt/lists/*
RUN pip3 install beautifulsoup4
RUN pip3 install requests
RUN wget https://gitlab.com/crocokyle/fxserver-base/-/raw/main/getLatestCFXBuild.py
RUN python3 getLatestCFXBuild.py
RUN tar xf fx.tar.xz -C ~/FXServer/server
RUN rm fx.tar.xz
RUN git clone https://github.com/citizenfx/cfx-server-data.git ~/FXServer/server-data
RUN rm -r -f /root/FXServer/server-data/resources
