# Description

Used to build the latest CFX Server base image from the latest stable version here https://runtime.fivem.net/artifacts/fivem/build_proot_linux/master/


```bash
git pull registry.gitlab.com/crocokyle/fxserver-base:latest
```

## Command to build the revolution2.0 image
```bash
docker build -t registry.gitlab.com/crocokyle/revolution-2-0-docker/revolution2.0:v1.0 .
```
## Command to run the final image
Make sure your local `server-data` folder contains a `server.cfg` and `permissions.cfg` file (if you want VMenu)
```cmd
docker run -p 30120:30120 -p 30120:30120/udp -ti -v C:\path\to\local\server-data:/home/FXServer/server-data registry.gitlab.com/crocokyle/revolution-2-0-docker/revolution2.0:latest
```
